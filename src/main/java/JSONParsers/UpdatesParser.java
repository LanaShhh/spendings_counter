package JSONParsers;

import TelegramInteractions.RequestHandler;
import org.json.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import java.util.logging.Logger;

public class UpdatesParser {
    private static Logger log = Logger.getLogger(UpdatesParser.class.getName());
    private static final int LIMIT = 3;
    private static int offset = -1;
    private static int curQueryLen = 0;
    private static int curId = 0;
    private static JSONArray curQuery = null;

    public static void GetUpdatesQuery(String content) {
        curQuery = JSONParser.GetResultUpdates(content);
    }

    public static JSONObject GetNewUpdate() throws Exception {
        if (curId >= curQueryLen) {
            if (offset == -1) {
                SetStartUpdateNum();
            }
            if (curQuery == null) {
                log.info("Query is empty");
            }
            GetUpdatesQuery(RequestHandler.GetRequest(RequestHandler.GET_UPDATES_COMMAND, offset + 1, LIMIT));
            curId = 0;
            curQueryLen = curQuery.length();
            log.info("Get new query, len is " + curQueryLen);
        }

        JSONObject cur = curQuery.getJSONObject(curId++);

        offset = JSONParser.GetUpdateID(cur);
        SaveStartUpdateNum();

        log.info(String.format("offset=%s, current update_id=%s", offset, JSONParser.GetUpdateID(cur)));

        return cur;
    }

    public static void SetStartUpdateNum() throws FileNotFoundException {
        File file = new File("src/main/java/JSONParsers/updateNum.txt");
        Scanner scanner = new Scanner(file);
        offset = scanner.nextInt();
        scanner.close();
    }

    public static void SaveStartUpdateNum()
            throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter writer =
                new PrintWriter("src/main/java/JSONParsers/updateNum.txt", "UTF-8");
        writer.println(offset);
        writer.close();
    }
}
