package JSONParsers;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONParser {
    public static Boolean GetStatus(String content) {
        JSONObject jsonObject = new JSONObject(content);
        return jsonObject.getBoolean("ok");
    }

    public static JSONArray GetResultUpdates(String content) {
        Boolean status = GetStatus(content);

        if (status) {
            JSONObject jsonObject = new JSONObject(content);
            return jsonObject.getJSONArray("result");
        } else {
            return null;
        }
    }

    public static String GetMessage(JSONObject object) {
        return object.getJSONObject("message").getString("text");
    }

    public static int GetId(JSONObject object) {
        return object.getJSONObject("message").getJSONObject("chat").getInt("id");
    }

    public static int GetUpdateID(JSONObject object) {
        return object.getInt("update_id");
    }
}
