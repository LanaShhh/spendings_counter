package TelegramInteractions;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

public class RequestHandler {
    public static final String GET_UPDATES_COMMAND = "getUpdates";
    public static final String SEND_MESSAGE_COMMAND = "sendMessage";
    private static String urlGetUpdates =
            new String("https://api.telegram.org/bot%s/%s");
    private static String urlGetUpdatesOffset =
            new String("https://api.telegram.org/bot%s/%s?offset=%s");
    private static String urlGetUpdatesOffsetLimit =
            new String("https://api.telegram.org/bot%s/%s?offset=%s&limit=%s");
    private static String urlSendMessage =
            new String("https://api.telegram.org/bot%s/%s?chat_id=%s&text=%s");
    private static URL MakeUrlGetUpdates(String methodName) throws MalformedURLException {
        return
                new URL(String.format(urlGetUpdates, System.getenv("BOT_TOKEN"), methodName));
    }
    private static URL MakeUrlGetUpdates(String methodName, int offset) throws MalformedURLException {
        return
                new URL(String.format(urlGetUpdatesOffset, System.getenv("BOT_TOKEN"), methodName, offset));
    }
    private static URL MakeUrlGetUpdates(String methodName, int offset, int limit) throws MalformedURLException {
        return
                new URL(String.format(urlGetUpdatesOffsetLimit,
                        System.getenv("BOT_TOKEN"), methodName, offset, limit));
    }
    private static URL MakeUrlSendMessage(String methodName, String chat_id, String text) throws MalformedURLException {
        return
                new URL(String.format(urlSendMessage, System.getenv("BOT_TOKEN"), methodName, chat_id, text));

    }
    public static String GetRequest(String methodName, int offset, int limit) throws Exception {
        try {
            final URL url = MakeUrlGetUpdates(methodName, offset);
            final HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");

            try (final BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String inputLine;
                final StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                return content.toString();
            } catch (final Exception ex) {
                ex.printStackTrace();
                return "";
            }
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    public static String PostRequest(String methodName, int chat_id, String text) throws Exception {
        try {
            final URL url = MakeUrlSendMessage(methodName, String.valueOf(chat_id), text);
            final HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            try (final BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String inputLine;
                final StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                return content.toString();
            } catch (final Exception ex) {
                ex.printStackTrace();
                return "";
            }
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
}
