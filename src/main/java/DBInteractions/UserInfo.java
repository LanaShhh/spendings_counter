package DBInteractions;

import JSONParsers.UpdatesParser;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserInfo {
    private static final String LOG_ADD_USER = "Adding user to users database";
    private static final String LOG_CREATE_TABLES = "Creating table %s";

    private static final String[] TABLE_NAMES = {"users", "receipts"};
    private static final Logger log = Logger.getLogger(UpdatesParser.class.getName());
    private static final String DBPATH = System.getenv("DB_PATH");

    public static final String QUERY_CREATE_TABLE_USERS = new StringBuilder()
            .append("CREATE TABLE IF NOT EXISTS users ")
            .append("(ID INT PRIMARY KEY NOT NULL,")
            .append("NAME char(50) NOT NULL)").toString();

    public static final String QUERY_CREATE_TABLE_RECEIPTS = new StringBuilder()
            .append("CREATE TABLE IF NOT EXISTS receipts ")
            .append("(ID INT NOT NULL,")
            .append("SUM INT NOT NULL,")
            .append("TYPE char(50) NOT NULL,")
            .append("DATE date NOT NULL)").toString();

    public static final String QUERY_ADD_USER = new StringBuilder()
            .append("INSERT INTO users ")
            .append("VALUES (%d, '%s')").toString();

    private static Connection OpenDatabase() {
        Connection connection = null;

        try {
            log.info("Opening database");
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection(String.format("jdbc:sqlite:%s", DBPATH));
            log.info("Opened database sucessfully");

            return connection;
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        return connection;
    }

    public static void CreateDatabase() {
        try {
            Connection connection = OpenDatabase();
            connection.close();
        } catch (java.sql.SQLException e) {
            log.log(Level.WARNING, e.getMessage());
        }
    }

    public static void CreateTables() {
        ExecuteQuery(QUERY_CREATE_TABLE_USERS, String.format(LOG_CREATE_TABLES, TABLE_NAMES[0]));
        ExecuteQuery(QUERY_CREATE_TABLE_RECEIPTS, String.format(LOG_CREATE_TABLES, TABLE_NAMES[1]));
    }

    public static void AddUSer(int id, String name) {
        ExecuteQuery(String.format(QUERY_ADD_USER, id, name), LOG_ADD_USER);
    }

    private static void ExecuteQuery(String query, String logMessage) {
        Connection connection = null;
        Statement statement = null;

        try {
            connection = OpenDatabase();

            log.info(logMessage);
            statement = connection.createStatement();
            statement.executeUpdate(query);
            statement.close();
            connection.close();
            log.info("Done!");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public static ResultSet GetRows() {
        return null;
    }
}
