import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import DBInteractions.UserInfo;
import JSONParsers.JSONParser;
import TelegramInteractions.RequestHandler;
import JSONParsers.UpdatesParser;
import org.json.JSONArray;
import org.json.JSONObject;

public class Bot {
    private static Logger log = Logger.getLogger(Bot.class.getName());
    public static void main(String[] args) throws Exception {
        Preprocessing();
        LoopGetMessages();
    }
    private static void LoopGetMessages() {
        while(true) {
            try {
                log.info("Getting new update");

                JSONObject curUpdate = UpdatesParser.GetNewUpdate();
                int id = JSONParser.GetId(curUpdate);
                String text = JSONParser.GetMessage(curUpdate);

                log.info(String.format("Get new msg, id=%d, text=%s", id, text));

                log.info("Answering message");
                RequestHandler.PostRequest(RequestHandler.SEND_MESSAGE_COMMAND, id, text);
            } catch (org.json.JSONException e) {
                log.info("No updates, sleep for 1 sec");
                try {
                    Thread.sleep(1000);
                } catch ( java.lang.InterruptedException ex) {
                    log.log(Level.WARNING, ex.getMessage());
                }
            } catch (java.lang.Exception e) {
                log.log(Level.WARNING, e.getMessage());
            }
        }
    }

    private static void Preprocessing() {
        UserInfo.CreateDatabase();
        UserInfo.CreateTables();
    }
    private static void LoopEcho() {
        while (true) {
            try {
                log.info("Getting new update");

                JSONObject curUpdate = UpdatesParser.GetNewUpdate();
                int id = JSONParser.GetId(curUpdate);
                String text = JSONParser.GetMessage(curUpdate);

                log.info(String.format("Get new msg, id=%d, text=%s", id, text));
                log.info("Answering message");
                RequestHandler.PostRequest(RequestHandler.SEND_MESSAGE_COMMAND, id, text);
            } catch (org.json.JSONException e) {
                log.info("No updates, sleep for 1 sec");
                try {
                    Thread.sleep(1000);
                } catch ( java.lang.InterruptedException ex) {
                    log.log(Level.WARNING, ex.getMessage());
                }
            } catch (java.lang.Exception e) {
                log.log(Level.WARNING, e.getMessage());
            }
        }
    }
}
